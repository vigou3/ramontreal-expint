# Présentation

Ce dépôt contient les diapositives de la présentation «Programmer pour
collaborer: utilisation et conception d'une interface de programmation
applicative (API)» prononcée dans le cadre du colloque [R à
Montréal](http://rmontreal2018.ca), le 5 juillet 2018.

## Auteur

[Vincent Goulet](https://vgoulet.act.ulaval.ca/), École d'actuariat,
Université Laval

## Résumé

Si vous avez déjà intégré du code C/C++ dans votre paquetage, vous
savez que R rend disponible aux développeurs un grand nombre de
fonctions internes C et Fortran par le biais d'une interface de
programmation applicative (API). Ce qui est peut-être moins bien
connu, c'est que votre paquetage peut autant importer les
fonctionnalités d'un autre paquetage qu'exporter les siennes propres.
Nous expliquerons comme procéder à l'aide de notre paquetage
[**expint**](https://cran.r-project.org/package=expint), qui fournit
des fonctions pour calculer les fonctions exponentielle intégrale et
gamma incomplète.

## Diapositives

Pour obtenir les diapositives en format PDF, consulter la [liste des versions](https://gitlab.com/vigou3/ramontreal-expint/tags).

## Historique des versions

### 1.0 (2018-07-05)

Version utilisée pour la présentation.


