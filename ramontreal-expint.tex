%%% Copyright (C) 2018 Vincent Goulet
%%%
%%% Ce fichier et tous les fichiers .tex ou .Rnw dont la racine est
%%% mentionnée dans les commandes \include ci-dessous font partie de
%%% «Programmer pour collaborer: utilisation et conception d'une
%%% interface de programmation applicative (API) - R à Montréal
%%% 2018»
%%% http://gitlab.com/vigou3/ramontreal-expint
%%%
%%% Cette création est mise à disposition selon le contrat
%%% Attribution-Partage dans les mêmes conditions 4.0
%%% International de Creative Commons.
%%% http://creativecommons.org/licenses/by-sa/4.0/

\documentclass[aspectratio=1610,10pt,xcolor=x11names,english,french]{beamer}
  \usepackage{babel}
  \usepackage[autolanguage]{numprint}
  \usepackage[round]{natbib}             % references
  \usepackage[noae]{Sweave}
  \usepackage{fontawesome}
  \usepackage{changepage}                % page licence
  \usepackage{tabularx}                  % page licence
  \usepackage{listings}                  % code source
  \usepackage{framed}                    % env. leftbar
  \usepackage{dirtree}                   % arbre de fichiers
  \usepackage[pstarrows]{pict2e}         % diagrammes
  \usepackage[overlay,absolute]{textpos} % couvertures
  \usepackage{metalogo}                  % logo \XeLaTeX

  %% =============================
  %%  Informations de publication
  %% =============================
  \renewcommand{\year}{2018}

  %% =======================
  %%  Apparence du document
  %% =======================

  %% Thème de beamer
  \usetheme{metropolis}

  %% Mathématiques en arev et ajustement de la taille des autres
  %% polices Fira; https://tex.stackexchange.com/a/405211/24355
  \usepackage{arevmath}
  \setsansfont[%
    BoldFont = {Fira Sans SemiBold},
    ItalicFont = {Fira Sans Book Italic},
    BoldItalicFont = {Fira Sans SemiBold Italic}]{Fira Sans Book}

  %% Couleurs additionnelles
  \definecolor{comments}{rgb}{0.7,0,0}      % commentaires
  \definecolor{link}{rgb}{0,0.4,0.6}        % liens internes
  \definecolor{url}{rgb}{0.6,0,0}           % liens externes
  \definecolor{rouge}{rgb}{0.85,0,0.07}     % rouge bandeau identitaire
  \definecolor{or}{rgb}{1,0.8,0}            % or bandeau identitaire
  \colorlet{codebg}{LightYellow1}           % fond code R
  \colorlet{alert}{mLightBrown} % alias for Metropolis color

  %% Hyperliens
  \hypersetup{%
    pdfauthor = {Vincent Goulet},
    pdftitle = {Programmer pour collaborer: utilisation et conception d'une
      interface de programmation applicative (API) - R à Montréal 2018},
    colorlinks = {true},
    linktocpage = {true},
    allcolors = {link},
    urlcolor = {url},
    pdfpagemode = {UseOutlines},
    pdfstartview = {Fit},
    bookmarksopen = {true},
    bookmarksnumbered = {true},
    bookmarksdepth = {subsection}}

  %% Paramétrage de babel pour les guillemets
  \frenchbsetup{og=«, fg=»}

  %% Sections de code source
  \lstloadlanguages{R}
  \lstset{language=R,
    extendedchars=true,
    basicstyle=\small\ttfamily\NoAutoSpacing,
    commentstyle=\color{comments}\slshape,
    keywordstyle=\mdseries,
    escapeinside=`',
    aboveskip=0pt,
    belowskip=0pt,
    showstringspaces=false}

  %% Bibliographie
  \renewcommand{\newblock}{}    % https://tex.stackexchange.com/a/1971/24355
  \renewcommand{\bibsection}{}  % drop \section heading
  \bibliographystyle{francais}

  %% =========================
  %%  Nouveaux environnements
  %% =========================

  %% Environnements de Sweave.
  %%
  %% Les environnements Sinput et Soutput utilisent Verbatim (de
  %% fancyvrb). On les réinitialise pour enlever la configuration par
  %% défaut de Sweave, puis on réduit l'écart entre les blocs Sinput
  %% et Soutput.
  \DefineVerbatimEnvironment{Sinput}{Verbatim}{}
  \DefineVerbatimEnvironment{Soutput}{Verbatim}{}
  \fvset{fontsize=\small,listparameters={\setlength{\topsep}{0pt}}}

  %% L'environnement Schunk est complètement redéfini en un hybride
  %% des environnements snugshade* et leftbar de framed.
  \makeatletter
  \renewenvironment{Schunk}{%
    \def\FrameCommand##1{\hskip\@totalleftmargin
      \vrule width 3pt\colorbox{codebg}{\hspace{5pt}##1}%
      % There is no \@totalrightmargin, so:
      \hskip-\linewidth \hskip-\@totalleftmargin \hskip\columnwidth}%
    \MakeFramed {\advance\hsize-\width
      \@totalleftmargin\z@ \linewidth\hsize
      \advance\labelsep\fboxsep
      \@setminipage}%
  }{\par\unskip\@minipagefalse\endMakeFramed}
  \makeatother

  %% =====================
  %%  Nouvelles commandes
  %% =====================

  %% Noms de fonctions, code, environnement, etc.
  \newcommand{\file}[1]{\colorbox{Snow2}{\texttt{#1}}}
  \newcommand{\pkg}[1]{\textbf{#1}}

  %% Raccourcis usuels vg
  \newcommand{\esp}[1]{E [ #1 ]}

  %% Lien vers GitLab dans la page de notices
  \newcommand{\viewsource}[1]{%
    \href{#1}{\faGitlab\ Voir sur GitLab}}

  %%% =======
  %%%  Varia
  %%% =======

  %% Longueurs pour la composition des pages couvertures avant et
  %% arrière.
  \newlength{\banderougewidth} \newlength{\banderougeheight}
  \newlength{\bandeorwidth}    \newlength{\bandeorheight}
  \newlength{\imageheight}     \newlength{\imagewidth}
  \newlength{\logoheight}
  \newlength{\gapwidth}

%  \includeonly{couverture-avant,notices}

\begin{document}

%% frontmatter
\include{couverture-avant}
\include{notices}

%% mainmatter
\include{contenu}

%% backmatter
\include{bibliographie}
\include{colophon}
\include{couverture-arriere}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-engine: xetex
%%% TeX-master: t
%%% End:
